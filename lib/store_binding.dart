import 'package:flutter_part_1/controllers/store_controller.dart';
import 'package:flutter_part_1/services/provider.dart';
import 'package:flutter_part_1/services/repository.dart';
import 'package:get/get.dart';

class StoreBinding implements Bindings {
  @override
  void dependencies() {
    Get.lazyPut(() => StoreController(
        repository: ReviewRepository(storeProvider: StoreProvider())));
  }
}
