import 'package:flutter/material.dart';

class BottomBarText extends StatelessWidget {
  final String title;
  final String price;
  final double fontSize;
  final FontWeight fontWeight;
  final Color textColor;

  const BottomBarText({
    Key? key,
    required this.title,
    required this.price,
    required this.fontSize,
    required this.fontWeight,
    required this.textColor,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          title,
          style: TextStyle(
            fontWeight: fontWeight,
            fontSize: fontSize,
            color: textColor,
          ),
        ),
        const Spacer(),
        Text(
          price,
          style: TextStyle(
            fontWeight: fontWeight,
            fontSize: fontSize,
            color: textColor,
          ),
        ),
      ],
    );
  }
}
