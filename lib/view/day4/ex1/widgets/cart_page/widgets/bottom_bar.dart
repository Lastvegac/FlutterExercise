import 'package:flutter/material.dart';
import 'package:flutter_part_1/utils/size_config.dart';
import 'package:flutter_part_1/view/day4/ex1/components/separator.dart';
import 'package:flutter_part_1/view/day4/ex1/widgets/cart_page/widgets/bottom_bar_widget/3d_secure.dart';
import 'package:flutter_part_1/view/day4/ex1/widgets/cart_page/widgets/bottom_bar_widget/bottombar_text.dart';
import 'package:flutter_part_1/view/day4/ex1/widgets/cart_page/widgets/bottom_bar_widget/checkout_button.dart';

import '../../../configuration/food.dart';

class BottomBar extends StatelessWidget {
  final List<Food> foods;
  const BottomBar({Key? key, required this.foods}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var total = 0;
    for (var e in foods) {
      total += e.foodPrice;
    }

    var discount = 0;

    return Container(
      padding: EdgeInsets.symmetric(
        vertical: SizeConfig.screenHeight! / 15.0,
        horizontal: SizeConfig.screenHeight! / 30.0,
      ),
      // height: 174,
      decoration: const BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.only(
          topLeft: Radius.circular(30),
          topRight: Radius.circular(30),
        ),
      ),
      child: SafeArea(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Padding(
                padding:
                    EdgeInsets.only(bottom: SizeConfig.screenHeight! / 85.37),

                /// 8.0
                child: const MySeparator(
                  color: Colors.grey,
                )),
            const ThreeDSecure(),
            SizedBox(
              height: SizeConfig.screenHeight! / 45.54,
            ),

            /// 15.0
            BottomBarText(
                title: "Subtotal",
                price: "\$$total",
                fontSize: SizeConfig.screenHeight! / 45.54,
                fontWeight: FontWeight.w400,
                textColor: Colors.black54),

            /// 15
            SizedBox(
              height: SizeConfig.screenHeight! / 45.54,
            ),
            BottomBarText(
              title: "Discount",
              price: "\$$discount",
              fontSize: SizeConfig.screenHeight! / 45.54,
              fontWeight: FontWeight.w400,
              textColor: Colors.black54,
            ),
            SizedBox(
              height: SizeConfig.screenHeight! / 45.54,
            ),
            BottomBarText(
              title: "Total",
              price: "\$${total - discount}",
              fontSize: SizeConfig.screenHeight! / 37.95,
              fontWeight: FontWeight.bold,
              textColor: Colors.black,
            ),

            /// 18
            SizedBox(height: SizeConfig.screenHeight! / 34.15),

            /// 20.0
            const CheckoutButton(),
          ],
        ),
      ),
    );
  }
}
