import 'package:flutter/material.dart';
import 'package:flutter_part_1/utils/size_config.dart';
import 'package:flutter_part_1/view/day4/ex1/components/food_image.dart';
import 'package:flutter_part_1/view/day4/ex1/components/food_text.dart';
import 'package:flutter_part_1/view/day4/ex1/configuration/food.dart';

class FoodListWidget extends StatefulWidget {
  final List<Food> foodSelected;
  const FoodListWidget({Key? key, required this.foodSelected})
      : super(key: key);

  @override
  _FoodListWidgetState createState() => _FoodListWidgetState();
}

class _FoodListWidgetState extends State<FoodListWidget> {
  @override
  Widget build(BuildContext context) {
    List<Food>? foodSelected =
        ModalRoute.of(context)!.settings.arguments as List<Food>;

    void _handleRemoveItem(index) {
      List<Food>? tmp = foodSelected;
      if (tmp != null) tmp.removeAt(index);
      setState(() {
        foodSelected = tmp;
      });
    }

    void _confirmDelete(name, index) {
      ScaffoldMessenger.of(context).showSnackBar(
        SnackBar(
          content: Text("Delete $name?"),
          action: SnackBarAction(
              label: "Yes",
              onPressed: () {
                _handleRemoveItem(index);
              }),
        ),
      );
    }

    return Padding(
      padding: EdgeInsets.symmetric(horizontal: SizeConfig.screenWidth! / 20),
      child: ListView.builder(
        itemCount: foodSelected?.length,
        itemBuilder: (context, index) {
          var food = foodSelected![index];
          return Padding(
            padding:
                EdgeInsets.symmetric(vertical: SizeConfig.screenHeight! / 68.3),

            /// 10.0
            child: Dismissible(
              key: UniqueKey(),
              direction: DismissDirection.endToStart,
              onDismissed: (direction) {},
              background: Container(
                padding: EdgeInsets.symmetric(
                    horizontal: SizeConfig.screenWidth! / 20.55),

                /// 20.0
                decoration: BoxDecoration(
                  color: const Color(0xFFFFE6E6),
                  borderRadius: BorderRadius.circular(15),
                ),
                child: Row(
                  children: const [Spacer(), Icon(Icons.delete_outline)],
                ),
              ),
              child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(30.0),
                    boxShadow: [
                      BoxShadow(
                        offset: const Offset(4, 6),
                        blurRadius: 4,
                        color: Colors.black.withOpacity(0.1),
                      )
                    ]),
                child: Row(
                  children: [
                    FoodImage(foodImage: food.foodImageName),
                    SizedBox(width: SizeConfig.screenWidth! / 20.55),

                    /// 20.0
                    FoodText(
                        foodName: food.foodName, foodPrice: food.foodPrice),
                    const Spacer(),
                    IconButton(
                        onPressed: () {
                          _confirmDelete(food.foodName, index);
                        },
                        icon: const Icon(
                          Icons.delete_outline,
                          color: Colors.black54,
                        ))
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
  }
}
