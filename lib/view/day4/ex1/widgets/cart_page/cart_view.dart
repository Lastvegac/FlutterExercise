import 'package:flutter/material.dart';
import 'package:flutter_part_1/view/day4/ex1/configuration/food.dart';
import 'package:flutter_part_1/view/day4/ex1/widgets/cart_page/widgets/bottom_bar.dart';
import 'package:flutter_part_1/view/day4/ex1/widgets/cart_page/widgets/food_list.dart';

class CartView extends StatelessWidget {
  const CartView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final foodSelected =
        ModalRoute.of(context)!.settings.arguments as List<Food>;

    // print(foodSelected.map((e) => e.toString()));

    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        centerTitle: true,
        leading: IconButton(
          onPressed: () {
            Navigator.pop(context);
          },
          icon: const Icon(Icons.arrow_back_ios),
        ),
        title: const Text(
          "My Cart",
          style: TextStyle(color: Colors.black),
        ),
        elevation: 0,
        backgroundColor: Colors.white,
        iconTheme: const IconThemeData(color: Colors.black),
      ),
      body: FoodListWidget(foodSelected: foodSelected),
      bottomNavigationBar: BottomBar(foods: foodSelected),
    );
  }
}
