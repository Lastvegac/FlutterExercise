import 'package:flutter/material.dart';
import 'package:flutter_part_1/view/day4/ex1/widgets/authentication/widgets/text_field.dart';
import 'widgets/forgot_password.dart';
import 'widgets/login_button.dart';
import 'widgets/logo.dart';
import 'widgets//text_signup.dart';

class LoginPageView extends StatefulWidget {
  const LoginPageView({Key? key}) : super(key: key);

  @override
  _LoginPageViewState createState() => _LoginPageViewState();
}

class _LoginPageViewState extends State<LoginPageView> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white,
        body: Container(
          child: SingleChildScrollView(
            child: Column(
              children: [
                LogoImage(),
                LoginTextField(),
                ForgotPassword(),
                LoginButonColor(),
                TextSignUp(),
              ],
            ),
          ),
        ));
  }
}
