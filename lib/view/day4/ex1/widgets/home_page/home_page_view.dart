import 'package:flutter/material.dart';
import 'package:flutter_part_1/utils/size_config.dart';
import 'package:flutter_part_1/view/day4/ex1/components/food_image.dart';
import 'package:flutter_part_1/view/day4/ex1/components/food_text.dart';
import 'package:flutter_part_1/view/day4/ex1/configuration/food.dart';
import 'package:flutter_part_1/view/day4/ex1/configuration/food_list.dart';
import 'package:flutter_part_1/view/day4/routes.dart';
import 'package:lottie/lottie.dart';

class HomePageView extends StatefulWidget {
  const HomePageView({Key? key}) : super(key: key);

  @override
  _HomePageViewState createState() => _HomePageViewState();
}

class _HomePageViewState extends State<HomePageView> {
  final List<Food> selectedFood = [];
  @override
  Widget build(BuildContext context) {
    SizeConfig().init(context);
    return Scaffold(
        appBar: AppBar(
          title: const Text('Food Menu'),
          actions: <Widget>[
            IconButton(
              icon: const Icon(
                Icons.shopping_cart,
                color: Colors.white,
              ),
              onPressed: () {
                Navigator.pushNamed(
                  context,
                  AppRoutes.CART,
                  arguments: selectedFood,
                );
              },
            )
          ],
        ),
        body: Padding(
          padding:
              EdgeInsets.symmetric(horizontal: SizeConfig.screenWidth! / 20),
          child: FutureBuilder<List<Food>>(
              future: bringTheFoods(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
                  var foodList = snapshot.data;
                  return ListView.builder(
                      itemCount: foodList?.length,
                      itemBuilder: (context, index) {
                        var food = foodList![index];
                        return Padding(
                          padding: EdgeInsets.symmetric(
                              vertical: SizeConfig.screenHeight! / 68.3),

                          /// 10.0

                          child: Dismissible(
                            key: UniqueKey(),
                            direction: DismissDirection.endToStart,
                            onDismissed: (direction) {
                              setState(() {});
                            },
                            background: Container(
                              padding: EdgeInsets.symmetric(
                                  horizontal: SizeConfig.screenWidth! / 20.55),

                              /// 20.0
                              decoration: BoxDecoration(
                                color: const Color(0xFFFFE6E6),
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: Row(
                                children: const [
                                  Spacer(),
                                  Icon(Icons.delete_outline)
                                ],
                              ),
                            ),
                            child: Container(
                              decoration: BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: BorderRadius.circular(30.0),
                                  boxShadow: [
                                    BoxShadow(
                                      offset: const Offset(0, 4),
                                      blurRadius: 4,
                                      color: Colors.black.withOpacity(0.1),
                                    )
                                  ]),
                              child: Row(
                                children: [
                                  FoodImage(foodImage: food.foodImageName),
                                  SizedBox(
                                      width: SizeConfig.screenWidth! / 20.55),

                                  /// 20.0
                                  FoodText(
                                      foodName: food.foodName,
                                      foodPrice: food.foodPrice),
                                  const Spacer(),
                                  TextButton(
                                    onPressed: () {
                                      selectedFood.add(food);
                                      // print(selectedFood.map((e) => e));
                                    },
                                    style: TextButton.styleFrom(
                                      primary: Colors.black,
                                    ),
                                    child: const Text(
                                      'ADD',
                                      style: TextStyle(fontSize: 14),
                                    ),
                                  )
                                ],
                              ),
                            ),
                          ),
                        );
                      });
                } else {
                  return SizedBox(
                      child: Center(
                    child: Lottie.network(
                        "https://assets10.lottiefiles.com/packages/lf20_peztuj79.json",
                        height: SizeConfig.screenHeight! / 6.83,

                        /// 100.0
                        width: SizeConfig.screenWidth! / 4.11,

                        /// 100.0
                        repeat: false),
                  ));
                }
              }),
        ));
  }
}
