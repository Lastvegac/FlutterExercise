class Food {
  int foodId;
  String foodName;
  String foodImageName;
  String foodCategory;
  int foodPrice;
  bool selected;

  Food({
    required this.foodId,
    required this.foodName,
    required this.foodImageName,
    required this.foodCategory,
    required this.foodPrice,
    required this.selected,
  });

  @override
  String toString() {
    return "{\n\"foodId\": $foodId, $foodName, $foodCategory, $foodPrice\n}";
  }
}
