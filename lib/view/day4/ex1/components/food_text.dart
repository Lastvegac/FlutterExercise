import 'package:flutter/material.dart';
import 'package:flutter_part_1/utils/size_config.dart';
import 'package:flutter_part_1/view/day4/ex1/components/colors.dart';

class FoodText extends StatelessWidget {
  final String foodName;
  final int foodPrice;

  const FoodText({
    Key? key,
    required this.foodName,
    required this.foodPrice,
  }) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Text(
          foodName,
          style: const TextStyle(color: Colors.black, fontSize: 14),
          maxLines: 2,
        ),
        SizedBox(height: SizeConfig.screenHeight! / 341.5),

        /// 2.0
        Text.rich(
          TextSpan(
            text: "\$$foodPrice",
            style: TextStyle(
                fontWeight: FontWeight.w600,
                color: buttonColor,
                fontSize: SizeConfig.screenHeight! / 37.95

                /// 18
                ),
            // children: [
            //   TextSpan(
            //       text: " x 1", style: Theme.of(context).textTheme.bodyText1),
            // ],
          ),
        ),
      ],
    );
  }
}
