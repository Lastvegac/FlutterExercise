import 'package:flutter/material.dart';

class AddButton extends StatefulWidget {
  Object food;
  AddButton({Key? key, required this.food}) : super(key: key);

  @override
  _AddButtonState createState() => _AddButtonState();
}

void _addItemToCart(String newText, int index) {
  // setState(() {
  //   food[index].selected = true;
  // });
}

class _AddButtonState extends State<AddButton> {
  @override
  Widget build(BuildContext context) {
    return IconButton(
        onPressed: () {
          // _addItemToCart(food.selected, index)
        },
        icon: const Icon(
          Icons.add,
          color: Colors.black54,
        ));
  }
}
