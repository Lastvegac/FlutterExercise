import 'package:flutter/material.dart';
import 'package:flutter_part_1/view/day4/ex1/widgets/authentication/login_page_view.dart';
import 'package:flutter_part_1/view/day4/ex1/widgets/cart_page/cart_view.dart';
import 'package:flutter_part_1/view/day4/ex1/widgets/favorite_page/favorite_page_view.dart';
import 'package:flutter_part_1/view/day4/ex1/widgets/home_page/home_page_view.dart';
import 'package:flutter_part_1/view/day4/ex1/widgets/profile_page/profile_page_view.dart';
import 'package:flutter_part_1/view/day4/ex1/widgets/search_page/search_page_view.dart';

class AppRoutes {
  static const LOGIN = '/';
  static const HOME = '/home';
  static const CART = '/cart';
  static const SEARCH = '/search';
  static const FAVORITE = '/favorite';
  static const PROFILE = '/profile';

  static Map<String, Widget Function(BuildContext)> routes = {
    LOGIN: (_) => const LoginPageView(),
    HOME: (_) => const HomePageView(),
    CART: (_) => const CartView(),
    SEARCH: (_) => const SearchPageView(),
    FAVORITE: (_) => const FavoritePageView(),
    PROFILE: (_) => const ProfilePageView(),
  };
}
